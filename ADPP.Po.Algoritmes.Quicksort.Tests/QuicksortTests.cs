using App.Po.Algoritmes.Quicksort.Quicksort;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace ADPP.Po.Algoritmes.Quicksort.Tests
{
    public class QuicksortTests
    {
        [Fact]
        public void Sorting_An_Negative_Valued_List_Should_Pass()
        {
            List<int> list = new List<int> { -78, -61, -59, -54, -40, -31, -27, -24, -10, 0, 1, 4, 5, 7, 9, 16, 23, 30, 31, 47, 56, 59, 73 };
            List<int> unsortedList = new List<int> { 73, 5, 56, -24, -78, 1, 7, -59, 59, -31, -40, -10, 9, 0, 30, -54, 16, 23, 31, -61, 47, -27, 4 };
            unsortedList.PerformQuicksort();
            Assert.True(Enumerable.SequenceEqual(unsortedList, list));
        }

        [Fact]
        public void Sorting_An_Already_Sorted_List_Should_Not_Fail()
        {
            List<int> list = new List<int> { 1, 4, 5, 7, 9, 16, 23, 30, 31, 47, 56, 59, 73 };
            List<int> unsortedList = new List<int> { 1, 4, 5, 7, 9, 16, 23, 30, 31, 47, 56, 59, 73 };
            unsortedList.PerformQuicksort();
            Assert.True(Enumerable.SequenceEqual(unsortedList, list));
        }

        [Fact]
        public void Unsorted_List_Equals_Sorted_List()
        {
            List<int> list = new List<int> { 1, 4, 5, 7, 9, 16, 23, 30, 31, 47, 56, 59, 73 };
            List<int> unsortedList = new List<int> { 31, 4, 9, 56, 47, 30, 59, 23, 73, 5, 16, 1, 7 };
            Assert.False(Enumerable.SequenceEqual(unsortedList, list));
        }

        [Fact]
        public void Sorting_An_List_Should_Pass()
        {
            List<int> list = new List<int> { 1, 4, 5, 7, 9, 16, 23, 30, 31, 47, 56, 59, 73 };
            List<int> unsortedList = new List<int> { 31, 4, 9, 56, 47, 30, 59, 23, 73, 5, 16, 1, 7 };
            unsortedList.PerformQuicksort();
            Assert.True(Enumerable.SequenceEqual(unsortedList, list));
        }

        [Fact]
        public void Sorting_An_List_With_Only_The_Same_Numbers_Should_Pass()
        {
            List<int> list = new List<int> { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };
            List<int> unsortedList = new List<int> { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };
            unsortedList.PerformQuicksort();
            Assert.True(Enumerable.SequenceEqual(unsortedList, list));
        }

        [Fact]
        public void Sorting_An_List_With_Only_One_Item_Should_Pass()
        {
            List<int> list = new List<int> { 1 };
            List<int> unsortedList = new List<int> { 1 };
            unsortedList.PerformQuicksort();
            Assert.True(Enumerable.SequenceEqual(unsortedList, list));
        }

        [Fact]
        public void Sorting_An_List_Without_Values_Should_Return_An_Empty_List()
        {
            List<int> list = new List<int>();
            List<int> unsortedList = new List<int>();
            unsortedList.PerformQuicksort();
            Assert.True(Enumerable.SequenceEqual(unsortedList, list));
        }

        [Fact]
        public void Sorting_A_Null_Reference_Should_Throw_A_Null_Reference_Exception()
        {
            List<int> list = new List<int> { 1, 4, 5, 7, 9, 16, 23, 30, 31, 47, 56, 59, 73 };
            List<int> unsortedList = null;
            Assert.Throws<NullReferenceException>(() => unsortedList.PerformQuicksort());
        }

        [Fact]
        public void Sorting_Comparable_Object_Is_Equal_To_Sorted_List()
        {
            List<Person> list = new List<Person> {
                new Person() {
                    Name = "Niels",
                    Age = 20
                }, new Person() {
                    Name = "Dens",
                    Age = 22
                }, new Person() {
                    Name = "Luc",
                    Age = 30
                }, new Person() {
                    Name = "Hans",
                    Age = 32
                }, new Person() {
                    Name = "Piet",
                    Age = 45
                }, new Person() {
                    Name = "Coen",
                    Age = 50
                }, new Person() {
                    Name = "Henk",
                    Age = 52
                }
            };
            List<Person> unsortedList = new List<Person> {
                list[0],
                list[2],
                list[1],
                list[5],
                list[6],
                list[3],
                list[4]
            };

            unsortedList.PerformQuicksort();
            Assert.True(Enumerable.SequenceEqual(unsortedList, list));
        }
    }
}
