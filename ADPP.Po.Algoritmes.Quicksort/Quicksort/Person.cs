using System;

namespace App.Po.Algoritmes.Quicksort.Quicksort
{
    public class Person : IComparable
    {
        /// <summary>
        /// The name of the student
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The age of the student
        /// </summary>
        public int Age { get; set; }

        /// <summary>
        /// Compare to for the student object based on the age.
        /// </summary>
        /// <param name="obj">The student object where we compare to</param>
        /// <returns>-1 for smaller, 0 for same, 1 for bigger.</returns>
        public int CompareTo(object obj)
        {
            Person s = (Person)obj;
            if (Age < s.Age)
                return -1;
            else if (Age > s.Age)
                return 1;
            return 0;
        }
    }
}