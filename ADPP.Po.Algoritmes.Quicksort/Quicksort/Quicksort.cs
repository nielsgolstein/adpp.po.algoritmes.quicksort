using System;
using System.Collections.Generic;

namespace App.Po.Algoritmes.Quicksort.Quicksort
{
    public static class Quicksort
    {
        /// <summary>
        /// Sorts the list
        /// </summary>
        /// <typeparam name="T">The type of the list to sort.</typeparam>
        /// <param name="toSortList">The list to sort</param>
        public static void PerformQuicksort<T>(this List<T> toSortList) where T : IComparable
        {
            // do validations here since We do not want to do this in the recursive loop due performance issues.
            if (toSortList == null)
                throw new NullReferenceException("To sort a list, please add a list to the method.");

            DoSort(toSortList);
        }

        ///<summary>
        /// Performs the actual quicksort algoritm.
        ///</summary>
        private static void DoSort<T>(List<T> toSortList) where T : IComparable
        {
            //Upon having 0 or 1 item, the sort is finished and we simply return.
            if (toSortList.Count <= 1)
                return;

            //Define new lists for left, same and larger.
            List<T> left = new List<T>();
            List<T> equal = new List<T>();
            List<T> right = new List<T>();

            //Grab a pivot
            T pivorObject = PickPivot(toSortList);

            //Loop all list items and devide them into left or right.
            for (int i = 0; i < toSortList.Count; i++)
            {
                //The item.
                T item = toSortList[i];

                int comparison = item.CompareTo(pivorObject);
                if (comparison == -1)
                    left.Add(item);
                else if (comparison == 1)
                    right.Add(item);
                else
                    equal.Add(item);

            }

            // Execute this PerformSort method again as recursion to split the list again. We keep doing this until we have ONE item left.
            DoSort(left);

            // Same for right.
            DoSort(right);

            #region Merge 
            // Merge the devided lists using a Clear and add range to merge the lists together using the recursion.
            // First we add left, than equal and than right since we know these are ordered now. This will
            // feed forward in the recursion until all listparts are merged.
            toSortList.Clear();
            toSortList.AddRange(left);
            toSortList.AddRange(equal);
            toSortList.AddRange(right);

            #endregion
        }

        ///<summary>
        ///Returns the pivot from the provided list.
        ///</summary>
        private static T PickPivot<T>(List<T> array) where T : IComparable
        {
            // Median of three
            T first = array[0];
            T middle = array[array.Count / 2];
            T last = array[array.Count - 1];

            // Find out which one is the biggest using the CompareTo method.
            int firstValue = first.CompareTo(middle) + first.CompareTo(last);
            int medianValue = middle.CompareTo(last) + middle.CompareTo(first);
            int lastValue = last.CompareTo(middle) + last.CompareTo(middle);

            if (firstValue > medianValue && medianValue > lastValue || firstValue < medianValue && medianValue < lastValue)
                return middle;
            else if (medianValue > lastValue && lastValue > firstValue || medianValue < lastValue && lastValue < firstValue)
                return last;
            else if (lastValue > firstValue && firstValue > medianValue || lastValue < firstValue && firstValue < medianValue)
                return first;
            return first;
        }
    }
}