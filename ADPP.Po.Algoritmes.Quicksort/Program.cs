﻿using App.Po.Algoritmes.Quicksort.Quicksort;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace ADPP.Po.Algoritmes.Quicksort
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Random list

            var list = new List<int>();
            Random randNum = new Random();
            int c = 1000;

            for (int i = 0; i < c; i++)
            {
                list.Add(randNum.Next(0, c + 1000));
            }

            #endregion

            List<int> l1 = list.ToList();

            l1.PerformQuicksort();

            Console.ReadLine();
        }
    }
}
